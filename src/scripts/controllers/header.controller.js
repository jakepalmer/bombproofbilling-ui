/**
 * Created by jake on 9/8/15.
 */
/// ==== Header Controller
"use strict";


angular.module("app")
.controller("HeadCtrl", ["$scope", "Fullscreen", function($scope, Fullscreen) {
    $scope.toggleFloatingSidebar = function() {
        $scope.floatingSidebar = $scope.floatingSidebar ? false : true;
        console.log("floating-sidebar: " + $scope.floatingSidebar);
    };

    $scope.goFullScreen = function() {
        if (Fullscreen.isEnabled())
            Fullscreen.cancel();
        else
            Fullscreen.all()
    };


}]);
