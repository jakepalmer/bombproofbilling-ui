/**
 * Created by jake on 9/8/15.
 */

/// ==== Dashboard Controller
    "use strict";


    angular.module("app")
.controller("DashboardCtrl", ["$scope", function($scope) {


    $scope.analyticsconfig = {
        data: {
            columns: [
                ['Network Load', 30, 100, 80, 140, 150, 200],
                ['CPU Load', 90, 100, 170, 140, 150, 50]
            ],
            type: 'spline',
            types: {
                'Network Load': 'bar'
            }
        },
        color: {
            pattern: ["#3F51B5",  "#38B4EE", "#4CAF50", "#E91E63"]
        },
        legend: {
            position: "inset"
        },
        size: {
            height: 330
        }
    };


    // ==== Usage Stats
    $scope.storageOpts = {
        size: 100,
        lineWidth: 2,
        lineCap: "square",
        barColor: "#E91E63"
    };
    $scope.storagePercent = 80;

    $scope.serverOpts = {
        size: 100,
        lineWidth: 2,
        lineCap: "square",
        barColor: "#4CAF50"
    };
    $scope.serverPercent = 35;

    $scope.clientOpts = {
        size: 100,
        lineWidth: 2,
        lineCap: "square",
        barColor: "#FDD835"
    };
    $scope.clientPercent = 54;


    // === browser share
    $scope.browserconfig = {
        data: {
            columns: [
                ["Chrome", 48.9],
                ["Firefox", 16.1],
                ["Safari", 10.9],
                ["IE", 17.1],
                ["Other",7]
            ],
            type: "donut",
        },
        size: {
            width: 260,
            height: 260
        },
        donut: {
            width: 50
        },
        color: {
            pattern: ["#3F51B5", "#4CAF50", "#f44336", "#E91E63", "#38B4EE"]
        }
    }
}]);